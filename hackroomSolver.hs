solveColumn :: [Int] -> Int -> [[Int]]
solveColumn [c1,c2,c3,c4,c5] x = [[a,b,c,d,e]|a<-set,b<-set,c<-set,d<-set,e<-set,a*c1 + b*c2 + c*c3 + d*c4 + e*c5 == x ]where set = [0,1]

multiplyArrays :: [Int] -> [Int] -> [Int]
multiplyArrays  a b = [ (a !! x ) * (b !! x) |  x<-[0,1..4]]

checkRow :: [[Int]] -> Int -> Int -> Int
checkRow [a,b,c,d,e] x y = if((a !! x) + (b !! x) + (c !! x) + (d !! x) + (e !! x) == y ) then 1 else 0

checkRows :: [[Int]] -> [[Int]] -> [Int] -> Int
checkRows [c1,c2,c3,c4,c5] [a,b,c,d,e] rsum = sum([(checkRow [(multiplyArrays a c1),(multiplyArrays c2 b),(multiplyArrays c c3),(multiplyArrays d c4),(multiplyArrays e c5)] x (rsum !! x)) | x<-[0,1..4]])

solveCube :: [Int] -> [Int] -> [Int] -> [Int] -> [Int] -> [Int] -> [Int] -> [[[Int]]]
solveCube c1 c2 c3 c4 c5 csum rsum  = [[a,b,c,d,e] | 1<2 , a<-csolve1,b<-csolve2,c<-csolve3,d<-csolve4,e<-csolve5,(checkRows [c1,c2,c3,c4,c5] [a,b,c,d,e] rsum) == 5]
            where csolve1 = solveColumn c1 (csum !! 0)
                  csolve2 = solveColumn c2 (csum !! 1)
                  csolve3 = solveColumn c3 (csum !! 2)
                  csolve4 = solveColumn c4 (csum !! 3)
                  csolve5 = solveColumn c5 (csum !! 4)